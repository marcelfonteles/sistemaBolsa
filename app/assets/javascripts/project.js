// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs

//= require jquery3
//= require tinymce



//= require plugins/moment/moment.min
//= require plugins/daterangepicker/daterangepicker
//= require plugins/formatter/formatter
//= require plugins/formatter/formatter.min
//= require plugins/formatter/jquery.formatter
//= require plugins/formatter/jquery.formatter.min
//= require plugins/formatter/scale.fix
//= require plugins/select2/select2.min
//= require plugins/jquerypriceformat/jquery.priceformat.min
//= require plugins/loading/loading
//= require plugins/datatable/jquery.dataTables.min
//= require bootstrap/bootstrap.min

