class AdminController < ApplicationController
  before_action :authenticate_admin!
  layout 'site'

  def index
    @projects = Project.all
  end
end
