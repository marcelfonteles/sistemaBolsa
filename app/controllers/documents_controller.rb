class DocumentsController < ApplicationController
  before_action :find_project
  def show
    respond_to do |format|
      format.pdf do
        filedir = "tmp/PD#{current_admin.id}F.pdf"
        @project.abstract = ActionController::Base.helpers.strip_tags(@project.abstract)

        pdf = Document.new(@project, filedir).render
        send_data pdf, filename: "projeto.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end

  private

  def find_project
    @project = Project.find(params[:id])
  end
end
