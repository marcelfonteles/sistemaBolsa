class ProjectsController < ApplicationController
  layout 'site'
  before_action :find_project, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, except: [:index, :show]

  def index
    @projects = Project.all
  end

  def show
  end

  def new
    @coordinators = Coordinator.all.first.Ori_Nome
    @project = Project.new
    @departaments = Departament.where("Dep_Local = ?", Place.first.Loc_Cod)
    @places = Place.all
    @areas = Area.all
    @options =[]
  end

  def create
    #@project = Project.new(project_params)
    
    coordinator_name = project_params[:coordinator_name]
    title = project_params[:title]
    subject_area = project_params[:subject_area]
    academic_unit = project_params[:academic_unit]
    departament = project_params[:departament]
    abstract = project_params[:abstract]
    keyword1 = project_params[:keyword1]
    keyword2 = project_params[:keyword2]
    keyword3 = project_params[:keyword3]
    keyword4 = project_params[:keyword4]
    start_date = project_params[:start_date]
    end_date = project_params[:end_date]
    financing = project_params[:financing]
    financing_is_public = project_params[:financing_is_public]
    financing_is_nacional = project_params[:financing_is_nacional]
    money_approved = project_params[:money_approved]
    organization = project_params[:organization]
    
    pocketAjeitando = project_params[:pocket].split(';')

    pocket = Array.new(project_params[:numberOfYears].to_i)
    for i in 1..project_params[:numberOfYears].to_i
      pocket[i-1] = pocketAjeitando[i-1].to_f 
    end
  
    @project = Project.new(coordinator_name: coordinator_name, title: title, subject_area: subject_area, academic_unit: academic_unit, departament: departament, abstract: abstract, keyword1: keyword1, keyword2: keyword2, keyword3: keyword3, keyword4: keyword4, start_date: start_date, end_date: end_date, financing: financing, financing_is_public: financing_is_public, financing_is_nacional: financing_is_nacional, money_approved: money_approved, pocket: pocket, organization: organization)

    if @project.save
      redirect_to project_path(@project), notice: 'Projeto cadastrado com sucesso.'
    else
      puts @project.errors.full_messages
      redirect_to new_project_path, notice: @project.errors.full_messages
    end
  end


  def edit
    @departaments = Departament.where("Dep_Local = ?", @project.academic_unit)#Place.first.Loc_Cod)
    @places = Place.all
    @areas = Area.all

    # Como tem o jquery para a formatação do dinheiro, fiz essa gambiarra para não perder uma casa decimal
    if @project.money_approved.round(1) == @project.money_approved
      @project.money_approved = @project.money_approved * 10
    end 
  end

  def update

    coordinator_name = project_params[:coordinator_name]
    title = project_params[:title]
    subject_area = project_params[:subject_area]
    academic_unit = project_params[:academic_unit]
    departament = project_params[:departament]
    abstract = project_params[:abstract]
    keyword1 = project_params[:keyword1]
    keyword2 = project_params[:keyword2]
    keyword3 = project_params[:keyword3]
    keyword4 = project_params[:keyword4]
    start_date = project_params[:start_date]
    end_date = project_params[:end_date]
    financing = project_params[:financing]
    financing_is_public = project_params[:financing_is_public]
    financing_is_nacional = project_params[:financing_is_nacional]
    money_approved = project_params[:money_approved]
    organization = project_params[:organization]
    
    pocketAjeitando = project_params[:pocket].split(';')

    pocket = Array.new(project_params[:numberOfYears].to_i)
    for i in 1..project_params[:numberOfYears].to_i
      pocket[i-1] = pocketAjeitando[i-1].to_f
    end

    if @project.update(coordinator_name: coordinator_name, title: title, subject_area: subject_area, academic_unit: academic_unit, departament: departament, abstract: abstract, keyword1: keyword1, keyword2: keyword2, keyword3: keyword3, keyword4: keyword4, start_date: start_date, end_date: end_date, financing: financing, financing_is_public: financing_is_public, financing_is_nacional: financing_is_nacional, money_approved: money_approved, pocket: pocket, organization: organization)
    
      redirect_to project_path(@project), notice: 'Projeto atualizado com sucesso.'
    else
      redirect_to edit_project_path(@project), notice: 'Não foi possível atualizar o projeto'
    end
  end

  def destroy
    if @project.destroy
      redirect_to admin_index_path, notice: 'Projeto apagado com sucesso.'
    else
      redirect_to @project, notice: 'Não foi possível apagar o projeto'
    end
  end

  # Dynamic select field on form
  def update_departaments
    @departaments = Departament.all.where("Dep_Local = ?", params[:Dep_Local])
    respond_to do |format|
      format.js
    end 
  end

  def update_coordinators
    @coordinators = Coordinator.all.sort_by{|p| p.Ori_Nome}.collect{|p| p.Ori_Nome }
    respond_to do |format|
      format.js
    end 
  end

private
  
  def find_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:coordinator_name, :title, :subject_area, :academic_unit, :departament, :abstract, :keyword1, :keyword2, :keyword3, :keyword4, :start_date, :end_date, :numberOfYears, :financing, :financing_is_public, :financing_is_nacional, :money_approved, :pocket, :codigo, :Dep_Local, :organization)
  end
  


end