module ProjectsHelper

  def coordinator_order
    Coordinator.all.sort_by{|p| p.Ori_Nome}.collect{|p| p.Ori_Nome }
  end

  def place_order
    Place.all.sort_by{|a| a.Loc_Descri}.collect{|a| [a.Loc_Descri, a.Loc_Cod] }
  end

  def conditional_fields(area = nil)
    Departament.all.select {|d| d.Dep_Local == area}
  end

  def find_centro(codigo)
    Place.where(Loc_Cod: codigo).first.Loc_Descri
  end  
end
