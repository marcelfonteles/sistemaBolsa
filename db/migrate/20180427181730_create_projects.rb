class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :batata
      t.text :title
      t.string :subject_area
      t.string :academic_unit
      t.string :departament
      t.text :abstract
      t.string :keyword1
      t.string :keyword2
      t.string :keyword3
      t.string :keyword4
      t.date :start_date
      t.date :end_date
      t.integer :numberOfYears
      t.boolean :financing
      t.boolean :financing_is_public
      t.boolean :financing_is_nacional
      t.float :money_approved
      t.string :pocket

      t.timestamps
    end
  end
end
