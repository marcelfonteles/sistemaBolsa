class CreatePlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :places do |t|
      t.string :Loc_Cod
      t.string :Loc_Descri

      t.timestamps
    end
  end
end
