class CreateAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :areas do |t|
      t.integer :Are_CodArea
      t.string :Are_Descricao
      t.string :Are_Status
      t.integer :Are_GrandeArea

      t.timestamps
    end
  end
end
