class AddOrganizationToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :organization, :string
  end
end
