# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180530180230) do

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "areas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "Are_CodArea"
    t.string "Are_Descricao"
    t.string "Are_Status"
    t.integer "Are_GrandeArea"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "coordinators", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "Ori_Cpf"
    t.string "Ori_Nome"
    t.string "Ori_Email"
    t.string "Ori_Celular"
    t.string "Ori_EndLattes"
    t.integer "Ori_Ppg"
    t.string "Ori_Vinculo"
    t.integer "Ori_Status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departaments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "Dep_Cod"
    t.string "Dep_descricao"
    t.string "Dep_Local"
    t.integer "Dep_Status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "places", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "Loc_Cod"
    t.string "Loc_Descri"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "coordinator_name"
    t.text "title"
    t.string "subject_area"
    t.string "academic_unit"
    t.string "departament"
    t.text "abstract"
    t.string "keyword1"
    t.string "keyword2"
    t.string "keyword3"
    t.string "keyword4"
    t.date "start_date"
    t.date "end_date"
    t.integer "numberOfYears"
    t.boolean "financing"
    t.boolean "financing_is_public"
    t.boolean "financing_is_nacional"
    t.float "money_approved", limit: 24
    t.string "pocket"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "organization"
  end

end
